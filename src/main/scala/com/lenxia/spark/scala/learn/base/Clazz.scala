package com.lenxia.spark.scala.learn.base

/**
  * Author : Lenxia
  * Created: 2017/6/15
  * Updated: 2017/6/15
  * Version: 0.0.0
  * Contact: 2219708253@qq.com
  * Desc   :
  */

/*
  scala中的class
 */
class Animal(name: String, age: Int) {
  var gender: Char = _
  
  /*
    辅助构造函数: 每一个辅助构造函数都需要继承一个构造参数
  */
  def this(name: String, age: Int, gender: Char) = {
    this(name: String, age: Int)
    this.gender = gender
  }
}

// ***********************************************************
/*
  scala中的访问权限:
  待续.....
  private[learn]：表示该类的访问包权限
 */
private[learn] class Person private(val id: Long, var name: String, salary: Double) {}

// Person 伴生对象(同一scala文件中名称相同)
object Person {
  def main(args: Array[String]): Unit = {
    val p = new Person(1, "tom", 10000)
    println("id:" + p.id)
    //        println(p.salary) // 构造函数中既没有用val或者var修饰,该属性访问级别是private[this]
  }
}

// ***********************************************************

// main
object Clazz {

}
