package com.lenxia.spark.scala.learn.base

/**
  * Author : Lenxia
  * Created: 2017/7/11
  * Updated: 2017/7/11
  * Version: 0.0.0
  * Contact: 2219708253@qq.com
  * Desc   : scala中的模式匹配
  */
case class People(name: String, age: Int)

object MatchCaseDemo {
  
  def demo1(x: String) = x match {
    case "lenxia" => println("I am lenxia")
    case "bob" => println("I am bob")
    case _ => println("I am robot") // 其他情况
  }
  
  def demo2(x: Any) = x match {
    case "lenxia" => println("I am lenxia")
    case "bob" => println("I am bob")
    case y: Int => 2 // 匹配类型
    case _ => println("I am robot") // 其他情况
  }
  
  // 匹配样例类
  def demo3() = {
    val p1 = People("lenxia", 20)
    val p2 = People("bob", 20)
    val p3 = People("tom", 20)
    
    for (p <- List(p1, p2, p3)) {
      p match {
        case People("lenxia", 20) => println("Hi, lenxia_20")
        case People("lenxia", 22) => println("Hi, lenxia_22")
        case _ => println("other...")
      }
    }
  }
  
  def main(args: Array[String]): Unit = {
    demo1("xxx")
  }
  
}
