package com.lenxia.spark.scala.learn.base

/**
  * Author  : Lenxia
  * Created : 2017/8/15
  * Updated : 2017/8/15
  * Version : 0.0.0
  * Contact : 2219708253@qq.com
  * 隐式参数demo
  */
class Dog {
  def eat(name: String) = println(name + " is eatting")
}

// 提供隐式参数转换的上下文
object DogContext {
  implicit val dog = new Dog()
}

object ImplicitArgsDemo {
  implicit val age_1 = 12
  //
  def eat(name: String)(implicit dog: Dog) = dog.eat(name)
  
  def printAge(name: String)(implicit age: Int)=println(name+" is "+age +" years old!!!!")
  
  def main(args: Array[String]): Unit = {
  
    printAge("小花") // 该函数不用引入，因为作用域都在用一个对象内【ImplicitArgsDemo】
  
    import DogContext._ // 由于隐式转换的参数不在同一个对象内【ImplicitArgsDemo】，所以需要引入提供隐式转换的对象DogContext
    eat("小花")
    
  }
}
