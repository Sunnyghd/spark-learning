package com.lenxia.spark.scala.learn.base

/**
  * Author  : Lenxia
  * Created : 2017/8/15
  * Updated : 2017/8/15
  * Version : 0.0.0
  * Contact : 2219708253@qq.com
  * scala 隐式转换：会在函数作用域中找有implicit关键字的方法，然后转换
  */

class Animal_(val name:String)
class Cat_(val name:String)

object ImplicitDemo {
  
  // [隐式方法] scala会试图寻找有implicit的方法。
  implicit def convert(obj:Object):Animal_ ={
   
   if (obj.getClass == classOf[Cat_]) {
     val cat = obj.asInstanceOf[Cat_] // 将obj转换成Cat_对象
     new Animal_(cat.name)
   }else{
     null
   }
  }
  
  
  def run(animal:Animal_)={
    println(animal.name+" is running")
  }
  
  def main(args: Array[String]): Unit = {
    
    // 隐式调用convert方法
    val cat = new Cat_("小花")
    run(cat)
  }
  
}
