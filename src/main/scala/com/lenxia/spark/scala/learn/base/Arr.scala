package com.lenxia.spark.scala.learn.base

/**
  * Author : Lenxia
  * Created: 2017/4/8
  * Updated: 2017/4/8
  * Version: 0.0.0
  * Contact: 2219708253@qq.com
  * Desc   : scala数组
  */

object Arr extends App{
  // 创建一个数组
  val arr = Array("hadoop","spark","hbase","hive")

  // 转换成新的集合
  val newArr = for ( i<- arr) yield i.toUpperCase

  newArr.foreach(e => println(e))

}
