package com.lenxia.spark.scala.learn.base

/**
  * Author : Lenxia
  * Created: 2017/6/15
  * Updated: 2017/6/15
  * Version: 0.0.0
  * Contact: 2219708253@qq.com
  * Desc   : scala 函数式编程
  *
  */
object Func {
  val factor: Int = 100
  /*
    函数闭包:
      闭包是一个函数，返回值依赖于声明在函数外部的一个或多个变量。
      闭包通常来讲可以简单的认为是可以访问一个函数里面局部变量的另外一个函数。
      例子如下：
   */
  val close_1 = (x: Int) => x + 10

  val close_2 = (x: Int) => x * factor

  /*
     偏函数与部分函数(偏应用函数)
     偏函数：是一个将类型A转为类型B的特质，scala中使用特质PartialFunction定义
     偏应用函数：是指在调用函数时，有意缺少部分参数的函数
   */

  //TODO
  
  /*
    Scala 函数柯里化(Currying)：
    1、将原来接受两个参数的函数变成新的接受一个参数的函数的过程。
    2、新的函数返回一个以原有第二个参数为参数的函数。
   */
  def add(x:Int)(y:Int) = x+y
  
  def main(args: Array[String]): Unit = {
//    print(close_1(1))
    println(add(1)(2))
  }

}
