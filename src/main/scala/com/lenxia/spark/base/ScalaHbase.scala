package com.lenxia.spark.base

import org.apache.hadoop.hbase.client.{Admin, Connection, ConnectionFactory}
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}

/**
  * Author : Lenxia
  * Created: 2017/6/16
  * Updated: 2017/6/16
  * Version: 0.0.0
  * Contact: 2219708253@qq.com
  * Desc   : scala操作hbase
  */

object ScalaHbase {

  def getConn(): Connection = {
    val hConf = HBaseConfiguration.create()
    hConf.set("hbase.zookeeper.property.clientPort", "2181")
    hConf.set("hbase.zookeeper.quorum", "ip1,ip2")

    //Connection 的创建是个重量级的工作，线程安全，是操作hbase的入口
    val conn = ConnectionFactory.createConnection(hConf)
    conn
  }

  def getAdmin(): Admin = {
    val admin = getConn().getAdmin
    admin
  }

  def createTab(tableName: String) = {
    val userTable: TableName = TableName.valueOf(tableName)
  }

  //  main 入口
  def main(args: Array[String]): Unit = {

  }
}
