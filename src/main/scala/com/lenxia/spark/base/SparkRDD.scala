package com.lenxia.spark.base

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext

/**
  * Author  : Lenxia
  * Created : 2017/7/20
  * Updated : 2017/7/20
  * Version : 0.0.0
  * Contact : 2219708253@qq.com
  * spark rdd 算子操作
  */
object SparkRDD {
  val conf = new SparkConf().setMaster("local[2]").setAppName("SparkRDD")
  val sc = new SparkContext(conf)
  val sqlCtx = new SQLContext(sc)
  
  /**
    * foreach 操作
    */
  def foreachOperator(): Unit = {
  
  }
  
  def main(args: Array[String]): Unit = {
    foreachOperator()
  }
}
